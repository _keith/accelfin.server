package com.accelfin.server;

import com.accelfin.server.messaging.Connections;
import com.accelfin.server.messaging.ConnectionsBuilder;
import com.accelfin.server.test.TestScheduler;
import com.esp.CurrentThreadRouterDispatcher;
import com.esp.DefaultRouter;
import com.esp.Router;
import com.esp.functions.Action1;

import java.util.HashMap;

class TestConnection {
    public TestConnection(String connectionKey, Action1<ConnectionsBuilder.ConnectionBuilder> onConfigure) {
        this.connectionKey = connectionKey;
        this.onConfigure = onConfigure;
    }

    public String connectionKey;
    public TestMqGateway mqGateway;
    public Action1<ConnectionsBuilder.ConnectionBuilder> onConfigure;
}

public class TestSystem {
    public Router<TestModel> Router;
    public Clock Clock;
    public TestScheduler TestScheduler;
    public TestModel Model;
    public Exception LastException;
    public Connections Connections;
    public ConnectionsBuilder ConnectionsBuilder;
    public HashMap<String, TestConnection> ConnectionsByKey;
    public TestSystem() {

        Router = new DefaultRouter<>(
                new CurrentThreadRouterDispatcher(),
                exception -> LastException = exception
        );
        TestScheduler = new TestScheduler();
        Clock = new Clock<>(Router, this.TestScheduler);
        ConnectionsByKey = new HashMap<>();
    }

public TestSystem addConnection(String connectionKey, Action1<ConnectionsBuilder.ConnectionBuilder> onConfigure) {
        TestConnection connection = new TestConnection(connectionKey,onConfigure);
        ConnectionsByKey.put(connectionKey, connection);
        return this;
    }

    public TestSystem createConnections(String serviceId, String serviceType) {
        ConnectionsBuilder builder = ConnectionsBuilder.create()
                .withConfiguration(serviceId, serviceType)
                .withClock(Clock)
                .withRouter(Router)
                .withSchedule(TestScheduler);
        for(TestConnection connection : ConnectionsByKey.values()) {
            builder.withConnection(
                    connection.connectionKey,
                    cb -> {
                        cb.withMqGateway(connection.mqGateway);
                        connection.onConfigure.call(cb);
                    }
            );
        }
        Connections = builder.build();
        return this;
    }

    public TestSystem createModel() {
        Model = new TestModel(Clock, Connections);
        return this;
    }
}
