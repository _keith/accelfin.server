package com.accelfin.server;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class BigDecimalUtilsTest {
    @Test
    public void greaterThan() throws Exception {
        assertTrue(BigDecimalUtils.greaterThan(BigDecimal.valueOf(5), BigDecimal.valueOf(4)));
        assertFalse(BigDecimalUtils.greaterThan(BigDecimal.valueOf(4), BigDecimal.valueOf(5)));
        assertFalse(BigDecimalUtils.greaterThan(BigDecimal.valueOf(5), BigDecimal.valueOf(5)));
    }

    @Test
    public void lessThan() throws Exception {
        assertTrue(BigDecimalUtils.lessThan(BigDecimal.valueOf(4), BigDecimal.valueOf(5)));
        assertFalse(BigDecimalUtils.lessThan(BigDecimal.valueOf(5), BigDecimal.valueOf(4)));
        assertFalse(BigDecimalUtils.lessThan(BigDecimal.valueOf(5), BigDecimal.valueOf(5)));
    }

    @Test
    public void equalto() throws Exception {
        assertFalse(BigDecimalUtils.equalto(BigDecimal.valueOf(4), BigDecimal.valueOf(5)));
        assertFalse(BigDecimalUtils.equalto(BigDecimal.valueOf(5), BigDecimal.valueOf(4)));
        assertTrue(BigDecimalUtils.equalto(BigDecimal.valueOf(5), BigDecimal.valueOf(5)));
    }

}