
import com.accelfin.server.TestBase;
import com.accelfin.server.messaging.*;
import com.accelfin.server.messaging.events.InboundMessageEvent;
import com.accelfin.server.messaging.operations.*;
import org.junit.Test;

class StubLogonResponse {}
class GetPrivateStuffRequest {}
class GetPrivateStuffResponse {}
class PriceUpdateResponse {}
class PriceRequest {}
class PriceResponse {}
class StubLogonRequest {}

public class ConnectionTests extends TestBase{
    private Connection _connection1;

//    @Before
//    public void setUp() {
//        TestSystem = new TestSystem()
//                .addConnection("connection1",
//                        cb -> {
//                            // cb.withOperation()
//                        })
//                .createModel();
//        _connection1 = TestSystem.Model.getConnections().getConnection("connection1");
//    }

    @Test
    public void serverApiDraft() {
        // does away with manual operation configuration
        // who owns operation status? -> an entity on the model

        /////////////////////////////////////////////////////////////////////////////////////////
        // SERVER SIDE APIS
        /////////////////////////////////////////////////////////////////////////////////////////

        // ** Request getStreamOperation ** //
        // For locally owned request -> n responses,
        ServerRequestStreamOperation<StubLogonRequest, StubLogonResponse> logonOperation = _connection1.exposeRequestStreamOperation(
                // OperationOwnership.OwnedByThisService, implied
                "logon", //
                // incoming request mapper: map the MessageEnvelope's payload
                // TODO can we get away with serialisation here and not manual mapping?
                requestPayload -> {
                    return new StubLogonRequest();
                }
        );
        logonOperation.requests().observe(
                // calling this message as sometimes it's an ack to kill the getStreamOperation
                // Maybe InboundMessage should be called ClientMessageNotification as sometimes it
                // is a result of the server detecting a timeout, thus no client message was actually sent
                (InboundMessageEvent<StubLogonRequest> message) -> {
                    if (message.isCompleted()) {
                        if (message.getCompletionReason() == CompletionReason.HEARTBEAT_FAILURE) {
                            // server killed the getStreamOperation due to heartbeat timeout
                        } else {
                            // client has killed the getStreamOperation explicitly,
                            // you'd clean up any stored subscriptions here ,
                            // in this instance payload would be null
                        }
                    } else {
                        // we create the response using the operation and the incomng message.
                        // this will wire up all the correct endpoint/operation information for the message
                        // Maybe OutboundMessage should be ServerMessageNotification?
                        logonOperation.sendResponse(message, new StubLogonResponse(), "Session1", "Session2");
                    }
                });
        logonOperation.setAvailable(true);

        // ** RPC ** //
        // RPC will have the same API as request getStreamOperation, however internally it'll
        // wire up the queues differently as it's effectively a private request->getStreamOperation operation.
        // internally requests will come on a special queue (from memory) and responses go out via the senders 'reply to' header.
        ServerRPCOperation<GetPrivateStuffRequest, GetPrivateStuffResponse> getPrivateStuffOperation = _connection1.exposeRPCOperation(
                // OperationOwnership.OwnedByThisService, implied
                "getPrivateStuff", //
                // incoming request mapper: map the MessageEnvelope's payload
                // TODO can we get away with serialisation here and not manual mapping?
                requestPayload -> {
                    return new GetPrivateStuffRequest();
                }
        );
        getPrivateStuffOperation.requests().observe((InboundMessageEvent<GetPrivateStuffRequest> message) -> {
            getPrivateStuffOperation.sendResponse(message, new GetPrivateStuffResponse());
        });
        // use getPrivateStuffOperation as you do a normal request-> getStreamOperation

        // ** Stream ** //
        // for getStreamOperation operations for no-request->response streams
        ServerStreamOperation<PriceUpdateResponse> getPricesOperation = _connection1.exposeStreamOperation(
                "getPrices"
        );
        getPricesOperation.setAvailable(true);
        //.. dispatch example:
        getPricesOperation.sendResponse(new PriceUpdateResponse(), "sessionToken1", "sessionToken1");
    }

    @Test
    public void clientApiDraft() {

        /////////////////////////////////////////////////////////////////////////////////////////
        // CLIENT SIDE APIS
        // Should have a similar API to the JS client
        /////////////////////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        // ** getRequestStreamOperation **
        // getRequestStreamOperation: internally depending on the operation configuration of the server,
        // this may be an RPC call or a call to a direct exchange
        ClientRequestStreamOperation<StubLogonRequest, StubLogonResponse> logonOperation = _connection1.getRequestStreamOperation(
                "ServiceType",
                "OperationName",
                messageEnvelope -> new StubLogonResponse(),
                true
        );
        logonOperation
                .sendRequest( new StubLogonRequest())
                .observe((InboundMessageEvent<StubLogonResponse> message) -> {
                    if (message.isCompleted()) {
                        if (message.getCompletionReason() == CompletionReason.HEARTBEAT_FAILURE) {

                        } else if (message.getCompletionReason() == CompletionReason.SERVER_CLOSED) {

                        }
                    }
                }
            );

        ////////////////////////////////////////////////////////////////////////
        // ** getStreamOperation **
        ClientStreamOperation<PriceResponse> priceOperation = _connection1.getStreamOperation(
                "ServiceType",
                "OperationName",
                messageEnvelope -> new PriceResponse(),
                true
        );
        priceOperation
                .responses()
                .observe((InboundMessageEvent<PriceResponse> message) -> {
                            if (message.isCompleted()) {
                                if (message.getCompletionReason() == CompletionReason.HEARTBEAT_FAILURE) {

                                } else if (message.getCompletionReason() == CompletionReason.SERVER_CLOSED) {

                                }
                            }
                        }
                );
    }
}
