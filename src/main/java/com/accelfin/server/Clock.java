package com.accelfin.server;

import com.accelfin.server.messaging.events.ClockTickEvent;
import com.accelfin.server.messaging.events.InitEvent;
import com.esp.ObserveEvent;
import com.esp.Router;
import com.esp.disposables.DisposableBase;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.concurrent.TimeUnit;

public class Clock<T> extends DisposableBase {
    private final Router<T> _router;
    private final Scheduler _scheduler;

    public Clock(Router<T> router, Scheduler scheduler) {
        _router = router;
        _scheduler = scheduler;
    }

    public Instant now() {
        return _scheduler.now();
    }

    public LocalDate localDateNow() {
        return  now().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public long epochNowMs() {
        return TimeUnit.MICROSECONDS.convert(now().getEpochSecond(), TimeUnit.SECONDS);
    }

    public long epochNowNanos() {
        return TimeUnit.NANOSECONDS.convert(now().getEpochSecond(), TimeUnit.SECONDS);
    }

    public void observeEvents() {
        addDisposable(_router.observeEventsOn(this));
    }

    @ObserveEvent(eventClass = InitEvent.class)
    public void onInitEvent() {
        _scheduler.schedule(this::publishClockTickEvent, 100);
    }

    private void publishClockTickEvent() {
        if(isDisposed()) return;
        _router.publishEvent(new ClockTickEvent());
        _scheduler.schedule(this::publishClockTickEvent, 100);
    }
}
