package com.accelfin.server;

import java.math.BigDecimal;

public class BigDecimalUtils {
    public static boolean greaterThan(BigDecimal left, BigDecimal right) {
        return left.compareTo(right) > 0;
    }

    public static boolean greaterThanOrEqualTo(BigDecimal left, BigDecimal right) {
        return left.compareTo(right) >= 0;
    }

    public static boolean lessThan(BigDecimal left, BigDecimal right) {
        return left.compareTo(right) < 0;
    }

    public static boolean equalto(BigDecimal left, BigDecimal right) {
        return left.compareTo(right) == 0;
    }
}
