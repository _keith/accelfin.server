package com.accelfin.server.messaging.events;

public abstract class MessageEventBase implements MessagingEvent  {
    private String _connectionId;

    public MessageEventBase(String connectionId) {
        _connectionId = connectionId;
    }

    public String getConnectionId() {
        return _connectionId;
    }
}
