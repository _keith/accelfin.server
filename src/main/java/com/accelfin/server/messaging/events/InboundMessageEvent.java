package com.accelfin.server.messaging.events;

import com.accelfin.server.messaging.MessageEnvelope;

/**
 * note not all props on this type will be used depending upon what you're doing.
 * It's one of those things where a flat type is better than an abstraction for each specific type of inbound message
 */
public final class InboundMessageEvent<TPayload> {

    private MessageEnvelope _envelope;
    private TPayload _payload;

    public InboundMessageEvent(MessageEnvelope envelope, TPayload payload) {
        _envelope = envelope;
        _payload = payload;
    }

    // TODO limit the exposure to this, just tack the required props on this object
    public MessageEnvelope getEnvelope() {
        return _envelope;
    }

    public TPayload getPayload() {
        return _payload;
    }

    //    private SessionToken _sessionToken;
//    private String _operationName;
//    private String _correlationId;
//    private String _replyTo;
//    private boolean _isCompleted;
//    private Instant _timestamp;
//    private TPayload _payload;
//    private CompletionReason _completionReason;
//
//    private AuthenticationStatus _authenticationStatus = AuthenticationStatus.Unauthenticated;
//    private AuthenticationStatus _requiredAuthenticationStatus;
//
//    public boolean isCompleted() {
//        return _isCompleted;
//    }
//
//    public CompletionReason getCompletionReason() {
//        return _completionReason;
//    }
//
//    public SessionToken getSessionToken() {
//        return _sessionToken;
//    }
//
//    @Override
//    public String getConnectionId() {
//        return _sessionToken.getConnectionId();
//    }
//
//    public String getOperationName() {
//        return _operationName;
//    }
//
//    public String getCorrelationId() {
//        return _correlationId;
//    }
//
//    public String getReplyTo() {
//        return _replyTo;
//    }
//
//    public Instant getTimestamp() {
//        return _timestamp;
//    }
//
//    /**
//     * Gets the AuthenticationStatus of the current session that published this event
//     * @return
//     */
//    public AuthenticationStatus getAuthenticationStatus() {
//        return _authenticationStatus;
//    }
//
//    public void setAuthenticationStatus(AuthenticationStatus authenticationStatus) {
//        _authenticationStatus = authenticationStatus;
//    }
//
//    /**
//     * Gets the AuthenticationStatus for this trade.
//     * Typically anything coming in over the web connection will required auth, internal won't.
//     * However some operations will fit this, i.e. login (and perhaps various others)
//     */
//    public AuthenticationStatus getRequiredAuthenticationStatus() {
//        return _requiredAuthenticationStatus;
//    }
//
//    public TPayload getPayload() {
//        return _payload;
//    }
//
//    @Override
//    public String toString() {
//        return "InboundMessageEvent{" +
//                "event=" + getClass().getSimpleName() +
//                ", _sessionToken=" + _sessionToken +
//                ", _operationName='" + _operationName + '\'' +
//                ", _correlationId='" + _correlationId + '\'' +
//                ", _replyTo='" + _replyTo + '\'' +
//                ", _isCompleted=" + _isCompleted +
//                ", _authenticationStatus=" + _authenticationStatus +
//                '}';
//    }
}
