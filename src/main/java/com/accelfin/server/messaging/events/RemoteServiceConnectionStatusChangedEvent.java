package com.accelfin.server.messaging.events;

public class RemoteServiceConnectionStatusChangedEvent {
    private final String _serviceType;
    private final boolean _isConnected;

    public RemoteServiceConnectionStatusChangedEvent(String serviceType, boolean isConnected) {

        _serviceType = serviceType;
        _isConnected = isConnected;
    }

    public String getServiceType() {
        return _serviceType;
    }

    public boolean isConnected() {
        return _isConnected;
    }
}
