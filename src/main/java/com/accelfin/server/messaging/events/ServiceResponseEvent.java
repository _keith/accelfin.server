package com.accelfin.server.messaging.events;

import java.util.UUID;

public class ServiceResponseEvent  {
    private UUID _correlationId;

    public UUID getCorrelationId() {
        return _correlationId;
    }
}
