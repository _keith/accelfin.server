package com.accelfin.server.messaging.events;

public class MqConnectedEvent extends MessageEventBase {
    public MqConnectedEvent(String connectionId) {
        super(connectionId);
    }
}
