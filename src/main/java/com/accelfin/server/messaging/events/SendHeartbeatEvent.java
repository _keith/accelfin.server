package com.accelfin.server.messaging.events;

public class SendHeartbeatEvent extends MessageEventBase {
    private static int _counter = 0;

    public SendHeartbeatEvent(String connectionId) {
        super(connectionId);
        _counter++;
    }

    public int getCounter() {
        return _counter;
    }
}
