package com.accelfin.server.messaging;

import java.time.Instant;

import com.google.protobuf.Message;
import com.google.protobuf.TextFormat;

public class MessageEnvelope {
    private String _operationName;
    private Instant _timestamp;
    private Message _payload;
    private Boolean _hasCompleted;
    private String _error;
    private String _correlationId;
    private String _sessionId;
    private String _senderId;
    private String _connectionId;
    private String _replyTo;

    public String getOperationName() {
        return _operationName;
    }

    public void setOperationName(String operationName) {
        _operationName = operationName;
    }

    public Instant getTimestamp() {
        return _timestamp;
    }

    public void setTimestamp(Instant timestamp) {
        _timestamp = timestamp;
    }

    public String getCorrelationId() {
        return _correlationId;
    }

    public void setCorrelationId(String correlationId) {
        _correlationId = correlationId;
    }

    public <T extends Message> T getPayload() {
        return (T)_payload;
    }

    public void setPayload(Message payload) {
        _payload = payload;
    }

    public String getSessionId() {
        return _sessionId;
    }

    // should be set by the deserialiser
    public void setSessionId(String sessionId) {
        _sessionId = sessionId;
    }

    public Boolean getHasCompleted() {
        return _hasCompleted;
    }

    public void setHasCompleted(Boolean hasCompleted) {
        _hasCompleted = hasCompleted;
    }

    public String getError() {
        return _error;
    }

    public void setError(String error) {
        _error = error;
    }

    public String getSenderId() {
        return _senderId;
    }

    public void setSenderId(String senderId) {
        _senderId = senderId;
    }

    public String getConnectionId() {
        return _connectionId;
    }

    public void setConnectionId(String connectionId) {
        _connectionId = connectionId;
    }

    public String getReplyTo() {
        return _replyTo;
    }

    public void setReplyTo(String replyTo) {
        _replyTo = replyTo;
    }

    @Override
    public String toString() {
        return "MessageEnvelope{\n" +
                " _operationName='" + _operationName + "\'\n" +
                " _timestamp=" + _timestamp + "\n" +
                " _hasCompleted=" + _hasCompleted + "\n" +
                " _error='" + _error + "\'\n" +
                " _correlationId='" + _correlationId + "\'\n" +
                " _sessionId='" + _sessionId + "\'\n" +
                " _senderId='" + _senderId + "\'\n" +
                " _connectionId=" + _connectionId + "\n" +
                " _replyTo='" + _replyTo + "\'\n" +
                " _payload=" + (_payload == null ? "[null]" : TextFormat.printToString(_payload)) + "\n" +
                '}';
    }
}
