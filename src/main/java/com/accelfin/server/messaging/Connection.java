package com.accelfin.server.messaging;

import com.accelfin.server.AuthenticationStatus;
import com.accelfin.server.messaging.config.ConnectionConfig;
import com.accelfin.server.messaging.gateways.MqGateway;
import com.accelfin.server.messaging.operations.*;
import com.accelfin.server.messaging.status.RemoteServiceMonitor;
import com.accelfin.server.messaging.status.StatusMonitor;
import com.esp.Router;
import com.esp.functions.Func1;
import com.google.protobuf.Message;
import org.slf4j.Logger;

import java.util.ArrayList;

public class Connection extends MessagingBase {

    private Logger _logger;
    private MqGateway _mqGateway;
    private Router<Connection> _router;
    private InFlightRequests _inFlightRequests;
    private RemoteServiceMonitor _remoteServiceMonitor;
    private StatusMonitor _statusMonitor;
    private ConnectionConfig _connectionConfig;
    private ArrayList<OutboundMessage> _outboundMessages = new ArrayList<>();
    private OutboundMessageHandler _outboundMessageHandler;

    public Connection(
            Logger logger,
            MqGateway mqGateway,
            Router<Connection> router,
            InFlightRequests inFlightRequests,
            RemoteServiceMonitor remoteServiceMonitor,
            StatusMonitor statusMonitor,
            ConnectionConfig connectionConfig
    ) {
        super(connectionConfig.getConnectionId());

        _logger = logger;
        _mqGateway = mqGateway;
        _router = router;
        _inFlightRequests = inFlightRequests;
        _remoteServiceMonitor = remoteServiceMonitor;
        _statusMonitor = statusMonitor;
        _connectionConfig = connectionConfig;
        _outboundMessageHandler = outboundMessage -> { _outboundMessages.add(outboundMessage); };
    }

    public void preProcess() {
        _outboundMessages.clear();
    }

    public ArrayList<OutboundMessage> getOutboundMessages() {
        return _outboundMessages;
    }

    public void observeEvents() {
        _inFlightRequests.observeEvents();
        _remoteServiceMonitor.observeEvents();
        _statusMonitor.observeEvents();
        _mqGateway.observeEvents();
    }

    public void connect() {
        try {
            _logger.debug("connecting to mq");
            _mqGateway.connect();
        } catch (Exception e) {
            _logger.error("Error connecting to mq", e);
        }
    }

    public StatusMonitor getStatusMonitor() {
        return _statusMonitor;
    }

    public <TRequest extends Message, TResponse extends Message> ClientRequestStreamOperation<TRequest, TResponse> getRequestStreamOperation(
            String serviceType,
            String operationName,
            Func1<MessageEnvelope, TResponse> inboundResponseMapper,
            boolean waitForConnection
    ) {
        return new ClientRequestStreamOperation<>(
                _outboundMessageHandler,
                serviceType,
                operationName,
                inboundResponseMapper,
                waitForConnection
        );
    }

    public <TResponse extends Message> ClientStreamOperation<TResponse> getStreamOperation(
            String serviceType,
            String operationName,
            Func1<MessageEnvelope, TResponse> inboundResponseMapper,
            boolean waitForConnection
    ) {
        return new ClientStreamOperation<>(
                _outboundMessageHandler,
                serviceType,
                operationName,
                inboundResponseMapper,
                waitForConnection
        );
    }

    public <TRequest extends Message, TResponse extends Message> ServerRequestStreamOperation<TRequest, TResponse> exposeRequestStreamOperation(
            String operationName,
            Func1<MessageEnvelope, TRequest> inboundRequestMapper,
            AuthenticationStatus requiredAuthenticationStatus
    ) {
        return new ServerRequestStreamOperation<>(
                _router,
                _outboundMessageHandler,
                inboundRequestMapper,
                _connectionConfig,
                operationName,
                requiredAuthenticationStatus
        );
    }

    public <TRequest extends Message, TResponse extends Message> ServerRPCOperation<TRequest, TResponse> exposeRPCOperation(
            String operationName,
            Func1<MessageEnvelope, TRequest> inboundRequestMapper,
            AuthenticationStatus requiredAuthenticationStatus
    ) {
        return new ServerRPCOperation<>(
                _router,
                _outboundMessageHandler,
                inboundRequestMapper,
                _connectionConfig,
                operationName,
                requiredAuthenticationStatus
        );
    }

    public <TResponse extends Message> ServerStreamOperation<TResponse> exposeStreamOperation(
            String operationName
    ) {
        return new ServerStreamOperation<>(
                _outboundMessageHandler,
                _connectionConfig,
                operationName
        );
    }
}
