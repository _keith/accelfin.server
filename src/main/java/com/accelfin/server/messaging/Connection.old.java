//package com.accelfin.server.messaging;
//
//import com.accelfin.server.messaging.config.ConnectionConfig;
//import com.accelfin.server.messaging.config.operations.OperationConfig;
//import com.accelfin.server.messaging.events.InboundMessageEvent;
//import com.accelfin.server.messaging.gateways.MqGateway;
//import com.accelfin.server.messaging.status.RemoteServiceMonitor;
//import com.accelfin.server.messaging.status.StatusMonitor;
//import com.esp.Router;
//import com.esp.reactive.Observable;
//import com.google.protobuf.Message;
//import org.slf4j.Logger;
//
//import java.util.ArrayList;
//import java.util.Optional;
//
//public class Connection extends MessagingBase {
//
//    private Logger _logger;
//    private MqGateway _mqGateway;
//    private Router<Connection> _router;
//    private InFlightRequests _inFlightRequests;
//    private RemoteServiceMonitor _remoteServiceMonitor;
//    private StatusMonitor _statusMonitor;
//    private ConnectionConfig _connectionConfig;
//    private ArrayList<OutboundMessage> _outboundMessages = new ArrayList<>();
//
//    public Connection(
//            Logger logger,
//            MqGateway mqGateway,
//            Router<Connection> router,
//            InFlightRequests inFlightRequests,
//            RemoteServiceMonitor remoteServiceMonitor,
//            StatusMonitor statusMonitor,
//            ConnectionConfig connectionConfig
//    ) {
//        super(connectionConfig.getConnectionId());
//        _logger = logger;
//        _mqGateway = mqGateway;
//        _router = router;
//        _inFlightRequests = inFlightRequests;
//        _remoteServiceMonitor = remoteServiceMonitor;
//        _statusMonitor = statusMonitor;
//        _connectionConfig = connectionConfig;
//    }
//
//    public StatusMonitor getStatusMonitor() {
//        return _statusMonitor;
//    }
//
//    public void preProcess() {
//        _outboundMessages.clear();
//    }
//
//    public ArrayList<OutboundMessage> getOutboundMessages() {
//        return _outboundMessages;
//    }
//
//    public void observeEvents() {
//        _inFlightRequests.observeEvents();
//        _remoteServiceMonitor.observeEvents();
//        _statusMonitor.observeEvents();
//        _mqGateway.observeEvents();
//    }
//
//    public void connect() {
//        try {
//            _logger.debug("connecting to mq");
//            _mqGateway.connect();
//        } catch (Exception e) {
//            _logger.error("Error connecting to mq", e);
//        }
//    }
//
//    // TODO
//    public Observable<MessageEnvelope> getRequestStream(String operationName) {
//        return Observable.create(o -> {
//           // wire up observation via gateway
//        });
//    }
//
//    public void sendRequestMessage(
//            String serviceType,
//            String operationName,
//            String correlationId,
//            Message payload
//    ) {
//        Optional<OperationConfig> config = _remoteServiceMonitor.getOperationConfig(serviceType, operationName);
//        if (config.isPresent()) {
//            OperationConfig operationConfig = config.get();
//            _outboundMessages.add(
//                    new OutboundMessage(
//                            operationConfig.getOperationName(),
//                            correlationId,
//                            MessageDirection.Request,
//                            operationConfig,
//                            payload,
//                            null,
//                            null,
//                            null,
//                            null,
//                            _connectionConfig.getUserName()
//                    )
//            );
//        } else {
//            throw new RuntimeException(String.format("Service not connected. Can't send message for operation %s with correlationId %s", operationName, correlationId));
//        }
//    }
//
//    public void sendStreamResponseMessage(
//            String operationName,
//            Message payload,
//            boolean streamCompleted,
//            String... sessionTokens
//    ) {
//        OperationConfig operationConfig = _connectionConfig.getOperationConfig(operationName);
//        _outboundMessages.add(
//                new OutboundMessage(
//                        operationConfig.getOperationName(),
//                        null,
//                        MessageDirection.Response,
//                        operationConfig,
//                        payload,
//                        null,
//                        null,
//                        streamCompleted,
//                        null,
//                        sessionTokens
//                )
//        );
//    }
//
//    // TODO sendRpcRequest
//
//    public void sendRpcResponse(InboundMessageEvent initialEvent, Message payload) {
//        sendRpcResponse(
//                initialEvent.getOperationName(),
//                initialEvent.getCorrelationId(),
//                initialEvent.getReplyTo(),
//                payload,
//                initialEvent.getSessionToken().getSessionId()
//        );
//    }
//
//    public void sendRpcResponse(
//            String operationName,
//            String correlationId,
//            String replyTo,
//            Message payload,
//            String sessionToken
//    ) {
//        OperationConfig operationConfig = _connectionConfig.getOperationConfig(operationName);
//        _outboundMessages.add(
//                new OutboundMessage(
//                        operationName,
//                        correlationId,
//                        MessageDirection.Response,
//                        operationConfig,
//                        payload,
//                        replyTo,
//                        null,
//                        true,
//                        null,
//                        sessionToken
//                )
//        );
//    }
//}
