package com.accelfin.server.messaging.status;

import com.accelfin.server.Clock;
import com.accelfin.server.messaging.Connection;
import com.accelfin.server.messaging.config.operations.OperationConfig;
import com.accelfin.server.messaging.events.HeartbeatEvent;
import com.accelfin.server.messaging.events.RemoteServiceConnectionStatusChangedEvent;
import com.esp.Router;
import org.slf4j.Logger;

import java.util.HashMap;

public class RemoteService {
    private Logger _logger;
    private Router<Connection> _router;
    private Clock _clock;

    private HashMap<String, RemoteServiceInstance> _servicesById = new HashMap<>();

    public RemoteService(Logger logger, Router<Connection> router, Clock clock) {
        _logger = logger;
        _router = router;
        _clock = clock;
    }

    public void onHeartbeatReceived(HeartbeatEvent heartbeat) {
        // note we just take the first set of operation configurations
        RemoteServiceInstance serviceInstance = _servicesById.computeIfAbsent(
                heartbeat.getServiceId(),
                serviceId -> new RemoteServiceInstance(_clock, heartbeat.getServiceType(), serviceId, heartbeat.getOperationConfigs(), _logger)
        );
        updateServiceIsConnected(serviceInstance, true);
    }

    public void updateIsConnected() {
        _servicesById.values().forEach(service-> {
            updateServiceIsConnected(service, false);
        });
    }

    private void updateServiceIsConnected(RemoteServiceInstance serviceInstance, boolean heartbeatReceived) {
        boolean oldIsConnected = serviceInstance.getIsConnected();
        if(heartbeatReceived) {
            serviceInstance.updateLastSeenTimestamp();
        }
        serviceInstance.updateIsConnected();
        if (oldIsConnected != serviceInstance.getIsConnected()) {
            _router.publishEvent(new RemoteServiceConnectionStatusChangedEvent(serviceInstance.getServiceType(), serviceInstance.getIsConnected()));
        }
    }

    public boolean hasAvailableInstance() {
        return _servicesById.values().stream().anyMatch(RemoteServiceInstance::getIsConnected);
    }

    public boolean hasAvailableOperation(String operationName) {
        return _servicesById.values().stream()
                .filter(RemoteServiceInstance::getIsConnected)
                .map(s -> s.getOperationConfig(operationName))
                .anyMatch(OperationConfig::getIsAvailable);
    }

    public RemoteServiceInstance getAvailableInstance() {
        // just take the first connected instance for now
        return _servicesById.values().stream()
                .filter(RemoteServiceInstance::getIsConnected)
                .findFirst()
                .get();
    }
}
