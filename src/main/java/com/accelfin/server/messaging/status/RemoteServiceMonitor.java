package com.accelfin.server.messaging.status;

import com.accelfin.server.Clock;
import com.accelfin.server.messaging.Connection;
import com.accelfin.server.messaging.MessagingBase;
import com.accelfin.server.messaging.config.operations.OperationConfig;
import com.accelfin.server.messaging.events.ClockTickEvent;
import com.accelfin.server.messaging.events.HeartbeatEvent;
import com.esp.ObservationStage;
import com.esp.Router;
import org.slf4j.Logger;

import java.util.HashMap;
import java.util.Optional;

public class RemoteServiceMonitor extends MessagingBase {

    private Logger _logger;

    private Router<Connection> _router;
    private Clock _clock;
    private HashMap<String, RemoteService> _remoteServiceByServiceType = new HashMap<>();
    private long _lastCheckEpochMs;

    public RemoteServiceMonitor(
            Logger logger,
            Router<Connection> router,
            String connectionId,
            Clock clock
    ) {
        super(connectionId);
        _logger = logger;
        _router = router;
        _clock = clock;
    }

    public void observeEvents() {
        this.addDisposable(
                _router.beginObserveEvents()
                        .addObserver(HeartbeatEvent.class, this::onRemoteServiceHeartbeatEvent, this::isForThisConnection)
                        .addObserver(ClockTickEvent.class, this::onTimerEvent, ObservationStage.Committed)
                        .observe()
        );
    }

    private void onRemoteServiceHeartbeatEvent(HeartbeatEvent heartbeatEvent) {
        _logger.info("Processing heartbeat event [{}]. Config dto length [{}]", heartbeatEvent.getTimestamp(), heartbeatEvent.getOperationConfigs().length);
        RemoteService remoteService = _remoteServiceByServiceType.computeIfAbsent(heartbeatEvent.getServiceType(), serviceType -> new RemoteService(_logger, _router, _clock));
        remoteService.onHeartbeatReceived(heartbeatEvent);
    }

    public Optional<OperationConfig> getOperationConfig(String serviceType, String operationName) {
        if(!_remoteServiceByServiceType.containsKey(serviceType)) {
            return Optional.empty();
        }
        RemoteService remoteService = _remoteServiceByServiceType.get(serviceType);
        if(!remoteService.hasAvailableInstance()) {
            return Optional.empty();
        }
        OperationConfig operationConfig = remoteService.getAvailableInstance().getOperationConfig(operationName);
        return Optional.of(operationConfig);
    }

    private void onTimerEvent(ClockTickEvent e) {
        if(_clock.now().toEpochMilli()  > _lastCheckEpochMs + 1000) {
            _lastCheckEpochMs = _clock.now().toEpochMilli();
            _remoteServiceByServiceType.values().forEach(RemoteService::updateIsConnected);
        }
    }
}
