package com.accelfin.server.messaging;

import com.accelfin.server.messaging.gateways.MqGateway;
import com.esp.Router;
import org.slf4j.Logger;

public class InFlightRequests extends MessagingBase {

    private Logger _logger;

    private MqGateway _mqGateway;
    private Router<Connection> _router;

    public InFlightRequests(
            String connectionId,
            Logger logger,
            MqGateway mqGateway,
            Router<Connection> router
    ) {
        super(connectionId);
        _logger = logger;
        _mqGateway = mqGateway;
        _router = router;
    }

    public void observeEvents() {

    }
}
