package com.accelfin.server.messaging.gateways;

import com.accelfin.server.messaging.Connection;
import com.esp.Router;
import com.rabbitmq.client.ShutdownListener;
import com.rabbitmq.client.ShutdownSignalException;

public class MqShutdownListener implements ShutdownListener {
    private Router<Connection> _router;

    public MqShutdownListener(Router<Connection> router) {
        _router = router;
    }

    @Override
    public void shutdownCompleted(ShutdownSignalException cause) {

    }
}
