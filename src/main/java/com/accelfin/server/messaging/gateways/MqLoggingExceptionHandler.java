package com.accelfin.server.messaging.gateways;

import com.rabbitmq.client.*;
import org.slf4j.Logger;

import java.io.IOException;
import java.net.ConnectException;

// based on rabbitmq's ForgivingExceptionHandler
public class MqLoggingExceptionHandler implements ExceptionHandler {

    private Logger _logger;

    public MqLoggingExceptionHandler(Logger logger) {
        _logger = logger;
    }

    public void handleUnexpectedConnectionDriverException(Connection conn, Throwable exception) {
        _logger.error(this.getClass().getName() + ": handleUnexpectedConnectionDriverException", exception);
    }

    public void handleReturnListenerException(Channel channel, Throwable exception) {
        handleChannelKiller(channel, exception, "ReturnListener.handleReturn");
    }

    public void handleFlowListenerException(Channel channel, Throwable exception) {
        handleChannelKiller(channel, exception, "FlowListener.handleFlow");
    }

    public void handleConfirmListenerException(Channel channel, Throwable exception) {
        handleChannelKiller(channel, exception, "ConfirmListener.handle{N,A}ck");
    }

    public void handleBlockedListenerException(Connection connection, Throwable exception) {
        handleConnectionKiller(connection, exception, "BlockedListener");
    }

    public void handleConsumerException(Channel channel, Throwable exception,
                                        Consumer consumer, String consumerTag,
                                        String methodName)
    {
        handleChannelKiller(channel, exception, "Consumer " + consumer
                + " (" + consumerTag + ")"
                + " method " + methodName
                + " for channel " + channel);
    }

    /**
     * @since 3.3.0
     */
    public void handleConnectionRecoveryException(Connection conn, Throwable exception) {
        // ignore java.net.ConnectException as those are
        // expected during recovery and will only produce noisy
        // traces
        if (exception instanceof ConnectException) {
            // no-op
        } else {
            _logger.error(this.getClass().getName() + ": Caught an exception during connection recovery!", exception);
        }
    }

    /**
     * @since 3.3.0
     */
    public void handleChannelRecoveryException(Channel ch, Throwable exception) {
        _logger.error(this.getClass().getName() + ": Caught an exception when recovering channel " + ch.getChannelNumber(), exception);
    }

    /**
     * @since 3.3.0
     */
    public void handleTopologyRecoveryException(Connection conn, Channel ch, TopologyRecoveryException exception) {
        _logger.error(this.getClass().getName() + ": Caught an exception when recovering topology " + exception.getMessage(), exception);
    }

    protected void handleChannelKiller(Channel channel, Throwable exception, String what) {
        _logger.error(this.getClass().getName() + ": " + what + " threw an exception for channel " + channel + ":", exception);
    }

    protected void handleConnectionKiller(Connection connection, Throwable exception, String what) {
        _logger.error(this.getClass().getName() + ": " + what + " threw an exception for connection " + connection + ":", exception);
        try {
            connection.close(AMQP.REPLY_SUCCESS, "Closed due to exception from " + what);
        } catch (AlreadyClosedException ace) {
            // noop
        } catch (IOException ioe) {
            _logger.error(this.getClass().getName() + ": Failure during close of connection " + connection + " after " + exception + ":", ioe);
            connection.abort(AMQP.INTERNAL_ERROR, "Internal error closing connection for " + what);
        }
    }
}