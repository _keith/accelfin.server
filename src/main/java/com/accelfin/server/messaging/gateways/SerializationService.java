package com.accelfin.server.messaging.gateways;

import com.accelfin.server.messaging.MessageEnvelope;
import com.accelfin.server.messaging.dtos.MessageEnvelopeDto;
import com.accelfin.server.messaging.dtos.TimestampDto;
import org.slf4j.Logger;

import java.time.Instant;
import java.util.Base64;

public class SerializationService {

    private Logger _logger;

    public SerializationService(Logger logger) {
        _logger = logger;
    }

    public byte[] serializeToB64Bytes(MessageEnvelope envelope) throws Exception {
        MessageEnvelopeDto messageEnvelopeDto = mapToDto(envelope);
        String encoded = Base64.getEncoder().encodeToString(messageEnvelopeDto.toByteArray());
        return encoded.getBytes();
    }

    public MessageEnvelope deserializeFromB64Bytes(byte[] envelopeBytes) throws Exception {
        byte[] decoded = Base64.getDecoder().decode(envelopeBytes);
        MessageEnvelopeDto messageEnvelopeDto = MessageEnvelopeDto.newBuilder().mergeFrom(decoded).build();
        return mapFromDto(messageEnvelopeDto);
    }

    protected MessageEnvelope mapFromDto(MessageEnvelopeDto dto) throws Exception {
        MessageEnvelope envelope = new MessageEnvelope();
        envelope.setOperationName(dto.getOperationName());
        envelope.setTimestamp(Instant.ofEpochSecond(dto.getTimestamp().getSeconds()));
        envelope.setPayload(AnyDtoUtils.deserializePayloadMessage(dto.getPayload()));
        envelope.setHasCompleted(dto.getHasCompleted());
        envelope.setError(dto.getError());
        envelope.setCorrelationId(dto.getCorrelationId());
        envelope.setSessionId(dto.getSessionId());
        envelope.setSenderId(dto.getSenderId());
        return envelope;
    }

    protected MessageEnvelopeDto mapToDto(MessageEnvelope envelope) throws Exception {
        MessageEnvelopeDto.Builder builder = MessageEnvelopeDto.newBuilder();
        builder.setOperationName(envelope.getOperationName());
        builder.setTimestamp(TimestampDto.newBuilder().setSeconds(envelope.getTimestamp().getEpochSecond()));
        if(envelope.getPayload() != null) builder.setPayload(AnyDtoUtils.serializePayloadMessage(envelope.getPayload()));
        if(envelope.getHasCompleted() != null) builder.setHasCompleted(envelope.getHasCompleted()); // can be null for outgoing request
        if(envelope.getError() != null) builder.setError(envelope.getError());
        if(envelope.getCorrelationId() != null) builder.setCorrelationId(envelope.getCorrelationId());
        if(envelope.getSessionId() != null) builder.setSessionId(envelope.getSessionId());
        if(envelope.getSenderId() != null) builder.setSenderId(envelope.getSenderId());
        return builder.build();
    }
}
