package com.accelfin.server.messaging.properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;
import java.util.UUID;

public class ServiceProperties {

    private static Logger logger = LoggerFactory.getLogger(ServiceProperties.class);

    private static String _serviceType;
    private static String _serviceId;
    private HashMap<String, MqConnectionProperties> _connectionPropertiesByConnectionName = new HashMap<>();
    private final Properties _properties;

    public ServiceProperties(String resourceFileName) {
        logger.debug("Loading properties");
        _properties = new Properties();
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        try {
            _properties.load(classLoader.getResourceAsStream(resourceFileName));
            _serviceType = _properties.getProperty("service.type");
            _serviceId = String.format("%s.%s", _serviceType, UUID.randomUUID().toString());
            logServiceDetails();
        } catch (IOException e) {
            logger.error("Error loading properties", e);
            throw new RuntimeException("Could not load properties");
        }
    }

    public MqConnectionProperties getMqConnectionProperties(String connectionName) {
        MqConnectionProperties mqConnectionProperties = _connectionPropertiesByConnectionName.get(connectionName);
        if(mqConnectionProperties == null) {
            mqConnectionProperties = new MqConnectionProperties(_properties, connectionName);
            _connectionPropertiesByConnectionName.put(connectionName, mqConnectionProperties);
        }
        return mqConnectionProperties;
    }

    public String getServiceId() {
        return _serviceId;
    }

    public String getServiceType() {
        return _serviceType;
    }

    private void logServiceDetails(){
        logger.info("Service type:id => [{}:{}]", _serviceType, _serviceId);
    }
}