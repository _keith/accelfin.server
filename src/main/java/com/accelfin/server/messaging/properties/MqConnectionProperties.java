package com.accelfin.server.messaging.properties;

import java.util.Properties;

public class MqConnectionProperties {

    private Properties _properties;
    private String _connectionId;

    public MqConnectionProperties(Properties properties, String connectionId) {
        _properties = properties;
        _connectionId = connectionId;
    }

    public String getConnectionId() {
        return _connectionId;
    }

    public String getRabbitmqHost() {
        return _properties.getProperty(String.format("rabbitmq.%s.host", _connectionId));
    }

    public String getRabbitmqVHost() {
        return _properties.getProperty(String.format("rabbitmq.%s.vHost", _connectionId));
    }

    public String getRabbitmqUsername() {
        return _properties.getProperty(String.format("rabbitmq.%s.username", _connectionId));
    }

    public String getRabbitmqPassword() {
        return _properties.getProperty(String.format("rabbitmq.%s.password", _connectionId));
    }
}
