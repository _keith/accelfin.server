package com.accelfin.server.messaging;

import com.accelfin.server.messaging.events.MessagingEvent;
import com.esp.disposables.DisposableBase;

public abstract class MessagingBase extends DisposableBase {

    private String _connectionId;

    public MessagingBase(String connectionId) {
        _connectionId = connectionId;
    }

    public String getConnectionId() {
        return _connectionId;
    }

    protected boolean isForThisConnection(MessagingEvent messageEventBase) {
        return messageEventBase.getConnectionId().equals(_connectionId);
    }
}
