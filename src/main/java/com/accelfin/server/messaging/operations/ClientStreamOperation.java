package com.accelfin.server.messaging.operations;

import com.accelfin.server.messaging.MessageEnvelope;
import com.accelfin.server.messaging.OutboundMessageHandler;
import com.accelfin.server.messaging.events.InboundMessageEvent;
import com.esp.functions.Func1;
import com.esp.reactive.Observable;

public class ClientStreamOperation<TResponse> {
    public ClientStreamOperation(
            OutboundMessageHandler outboundMessageHandler,
            String serviceType,
            String operationName,
            Func1<MessageEnvelope, TResponse> inboundResponseMapper,
            boolean waitForConnection
    ) {
    }

    public Observable<InboundMessageEvent<TResponse>> responses() {
        return Observable.create(O -> {
            return () -> {};
        });
    }
}

