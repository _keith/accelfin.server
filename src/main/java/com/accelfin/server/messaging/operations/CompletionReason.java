package com.accelfin.server.messaging.operations;

public enum CompletionReason {
    HEARTBEAT_FAILURE,
    SERVER_CLOSED
}
