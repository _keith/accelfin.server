package com.accelfin.server.messaging.operations;

import com.accelfin.server.messaging.MessageDirection;
import com.accelfin.server.messaging.OutboundMessage;
import com.accelfin.server.messaging.OutboundMessageHandler;
import com.accelfin.server.messaging.config.ConnectionConfig;
import com.accelfin.server.messaging.config.operations.OperationConfig;
import com.accelfin.server.messaging.config.operations.OperationOwnership;
import com.accelfin.server.messaging.config.operations.OperationType;
import com.google.protobuf.Message;

public class ServerStreamOperation<TResponse extends Message> {
    private final OperationConfig _operationConfig;
    private OutboundMessageHandler _outboundMessageHandler;

    public ServerStreamOperation(
            OutboundMessageHandler outboundMessageHandler,
            ConnectionConfig connectionConfig,
            String operationName
    ) {
        _outboundMessageHandler = outboundMessageHandler;
        _operationConfig = connectionConfig.getOrAddOperation(
                OperationOwnership.OwnedByThisService,
                OperationType.RequestStream,
                operationName
        );
    }


    public void sendResponse(
            TResponse responsePayload,
            String... sessionTokens
    ) {
        _outboundMessageHandler.handle(
                new OutboundMessage(
                        _operationConfig.getOperationName(),
                        null,
                        MessageDirection.Response,
                        _operationConfig,
                        responsePayload,
                        null,
                        null,
                        false,
                        null,
                        sessionTokens
                )
        );
    }
}
