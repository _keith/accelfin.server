package com.accelfin.server.messaging.operations;

import com.accelfin.server.AuthenticationStatus;
import com.accelfin.server.messaging.*;
import com.accelfin.server.messaging.config.ConnectionConfig;
import com.accelfin.server.messaging.events.InboundMessageEvent;
import com.esp.Router;
import com.esp.functions.Func1;
import com.google.protobuf.Message;

public class ServerRPCOperation<TRequest, TResponse extends Message> extends ServerRequestStreamOperation<TRequest, TResponse> {

    public ServerRPCOperation(
            Router<Connection> router,
            OutboundMessageHandler outboundMessageHandler,
            Func1<MessageEnvelope, TRequest> inboundRequestMapper,
            ConnectionConfig connectionConfig,
            String operationName,
            AuthenticationStatus requiredAuthenticationStatus
    ) {
        super(router, outboundMessageHandler, inboundRequestMapper, connectionConfig, operationName, requiredAuthenticationStatus);
    }

    @Override
    public void sendResponse(
            InboundMessageEvent<TRequest> initialRequest,
            TResponse responsePayload,
            boolean streamCompleted,
            String... sessionTokens
    ) {
        getOutboundMessageHandler().handle(
                new OutboundMessage(
                        getOperationConfig().getOperationName(),
                        initialRequest.getEnvelope().getCorrelationId(),
                        MessageDirection.Response,
                        getOperationConfig(),
                        responsePayload,
                        initialRequest.getEnvelope().getReplyTo(),
                        null,
                        streamCompleted,
                        null,
                        initialRequest.getEnvelope().getSessionId()
                )
        );
    }
}
