package com.accelfin.server.messaging.operations;

import com.accelfin.server.AuthenticationStatus;
import com.accelfin.server.messaging.*;
import com.accelfin.server.messaging.config.ConnectionConfig;
import com.accelfin.server.messaging.config.operations.OperationConfig;
import com.accelfin.server.messaging.config.operations.OperationOwnership;
import com.accelfin.server.messaging.config.operations.OperationType;
import com.accelfin.server.messaging.events.InboundMessageEvent;
import com.esp.ObserveEvent;
import com.esp.Router;
import com.esp.disposables.DisposableBase;
import com.esp.functions.Func1;
import com.esp.reactive.Observable;
import com.esp.reactive.Subject;
import com.google.protobuf.Message;

// TODO All these need to add their configuration like the old code did
public class ServerRequestStreamOperation<TRequest, TResponse extends Message> extends DisposableBase {
    private Subject<InboundMessageEvent<TRequest>> _requestSubject = Subject.create();
    private Router<Connection> _router;
    private OutboundMessageHandler _outboundMessageHandler;
    private Func1<MessageEnvelope, TRequest> _inboundRequestMapper;
    private final OperationConfig _operationConfig;

    public ServerRequestStreamOperation(
            Router<Connection> router,
            OutboundMessageHandler outboundMessageHandler,
            Func1<MessageEnvelope, TRequest> inboundRequestMapper,
            ConnectionConfig connectionConfig,
            String operationName,
            AuthenticationStatus requiredAuthenticationStatus
    ) {
        _router = router;
        _outboundMessageHandler = outboundMessageHandler;
        _inboundRequestMapper = inboundRequestMapper;
        _operationConfig = connectionConfig.getOrAddOperation(
                OperationOwnership.OwnedByThisService,
                OperationType.RequestStream,
                operationName,
                requiredAuthenticationStatus == AuthenticationStatus.Authenticated
        );
    }

    public boolean isAvailable() {
        return _operationConfig.getIsAvailable();
    }

    public void setAvailable(boolean available) {
        _operationConfig.setIsAvailable(available);
    }

    protected OperationConfig getOperationConfig() {
        return _operationConfig;
    }

    protected OutboundMessageHandler getOutboundMessageHandler() {
        return _outboundMessageHandler;
    }

    public void observeEvents() {
        addDisposable(_router.observeEventsOn(this));
    }

    public Observable<InboundMessageEvent<TRequest>> requests() {
        return Observable.create(o -> {
            return _requestSubject.observe(o);
        });
    }

    public void sendResponse(
            InboundMessageEvent<TRequest> initialRequest,
            TResponse responsePayload,
            boolean streamCompleted,
            String... sessionTokens
    ) {
        _outboundMessageHandler.handle(
                new OutboundMessage(
                        getOperationConfig().getOperationName(),
                        null,
                        MessageDirection.Response,
                        _operationConfig,
                        responsePayload,
                        null,
                        null,
                        streamCompleted,
                        null,
                        sessionTokens
                )
        );
    }

    @ObserveEvent(eventClass = MessageEnvelope.class)
    public void onMessageEnvelopeReceived(MessageEnvelope envelope) {
        if(!envelope.getOperationName().equals(_operationConfig.getOperationName())) {
            return;
        }
        TRequest request = _inboundRequestMapper.call(envelope.getPayload());
        _requestSubject.onNext(new InboundMessageEvent<>(envelope, request));
    }
}
