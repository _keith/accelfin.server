package com.accelfin.server.messaging.mappers;

import com.accelfin.server.messaging.MessageEnvelope;
import com.accelfin.server.messaging.OperationNames;
import com.accelfin.server.messaging.config.operations.OperationConfig;
import com.accelfin.server.messaging.dtos.HeartbeatDto;
import com.accelfin.server.messaging.events.HeartbeatEvent;
import com.accelfin.server.messaging.events.InboundMessageEvent;

import java.time.Instant;

public class HeartbeatEventMapper {

    private ServiceStatusMapper _serviceStatusMapper;

    public HeartbeatEventMapper(ServiceStatusMapper serviceStatusMapper) {
        _serviceStatusMapper = serviceStatusMapper;
    }

    public InboundMessageEvent mapFromDto(MessageEnvelope envelope) {
        HeartbeatDto heartbeatDto = envelope.getPayload();
        HeartbeatEvent heartbeatEvent = new HeartbeatEvent(
                heartbeatDto.getServiceType(),
                heartbeatDto.getServiceId(),
                heartbeatDto.getOperationConfigDtosList().stream().map(_serviceStatusMapper::mapFromDto).toArray(OperationConfig[]::new),
                Instant.ofEpochMilli(heartbeatDto.getTimestamp().getSeconds())
        );
        MessageEnvelopeMapper.mapBaseFields(heartbeatEvent, envelope);
        return heartbeatEvent;
    }
}
