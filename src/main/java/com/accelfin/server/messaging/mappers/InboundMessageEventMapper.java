package com.accelfin.server.messaging.mappers;

import com.accelfin.server.messaging.MessageEnvelope;
import com.accelfin.server.messaging.events.InboundMessageEvent;

/**
 * Creates an inbound event for the given operation.
 * Allows us to bind specific methods within the model to typed events
 * and allows the router to dispatch to the methods in question.
 *
 * Without this we end up with a generic inbound event and there is no means
 * for the router to mapToDto this to specific methods, they'd all have to filter out unrelated events.
 *
 */
public interface InboundMessageEventMapper {
    InboundMessageEvent mapToEvent(MessageEnvelope envelope);
}
