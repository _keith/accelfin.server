package com.accelfin.server.messaging.mappers;

import com.accelfin.server.Clock;
import com.accelfin.server.messaging.config.ExchangeConst;
import com.accelfin.server.messaging.dtos.HeartbeatDto;
import com.accelfin.server.messaging.dtos.OperationConfigDto;
import com.accelfin.server.messaging.dtos.OperationTypeDto;
import com.accelfin.server.messaging.dtos.TimestampDto;
import com.accelfin.server.messaging.config.operations.MqNamingStrategy;
import com.accelfin.server.messaging.config.operations.OperationConfig;
import com.accelfin.server.messaging.config.operations.OperationOwnership;
import com.accelfin.server.messaging.config.operations.OperationType;

import java.util.Collection;
import java.util.HashMap;
import java.util.stream.Stream;

public class ServiceStatusMapper {

    private Clock _clock;

    public ServiceStatusMapper(Clock clock) {
        _clock = clock;
    }

    public HeartbeatDto mapToDto(String serviceType, String serviceId, Collection<OperationConfig> operationConfigs, HashMap<String, MqNamingStrategy> namingStrategies) {
        Stream<OperationConfigDto> operationConfigsDtos = operationConfigs.stream()
                .map(c -> this.mapToDto(c, namingStrategies.get(c.getOperationName())));
        return HeartbeatDto
                .newBuilder()
                .setServiceType(serviceType)
                .setServiceId(serviceId)
                .setTimestamp(TimestampDto.newBuilder().setSeconds(_clock.now().getEpochSecond()))
                .setServiceLoad(0)
                .addAllOperationConfigDtos(operationConfigsDtos::iterator)
                .build();
    }

    public OperationConfigDto mapToDto(OperationConfig operationConfig, MqNamingStrategy namingStrategy) {
        return OperationConfigDto.newBuilder()
                .setOperationName(operationConfig.getOperationName())
                .setOperationType(mapToDto(operationConfig.getOperationType()))
                .setRequiresAuthentication(operationConfig.getRequiresAuthentication())
                .setServiceType(operationConfig.getServiceType())
                .setServiceId(operationConfig.getServiceId())
                .setRequestExchangeName(ExchangeConst.REQUEST_EXCHANGE_NAME)
                .setResponseExchangeName(ExchangeConst.RESPONSE_EXCHANGE_NAME)
                .setRequestRoutingKey(namingStrategy.getRequestRoutingKey())
                .setResponseRoutingKey(namingStrategy.getResponseRoutingKey())
                .setIsAvailable(operationConfig.getIsAvailable())
                .build();
    }

    private OperationTypeDto mapToDto(OperationType operationType) {
        switch (operationType) {
            case RPC:
                return OperationTypeDto.RPC;
            case RequestStream:
                return OperationTypeDto.REQUEST_STREAM;
            case Stream:
                return OperationTypeDto.STREAM;
            default:
                throw new RuntimeException("Unknown operation type " + operationType);
        }
    }

    public OperationConfig mapFromDto(com.accelfin.server.messaging.dtos.OperationConfigDto dto) {
        return new OperationConfig(
                OperationOwnership.OwnedByRemoteService,
                mapFromDto(dto.getOperationType()),
                dto.getOperationName(),
                dto.getServiceType(),
                dto.getServiceId(),
                dto.getRequiresAuthentication(),
                dto.getIsAvailable()
        );
    }

    @SuppressWarnings("Duplicates")
    private OperationType mapFromDto(com.accelfin.server.messaging.dtos.OperationTypeDto operationType) {
        switch (operationType) {
            case RPC:
                return OperationType.RPC;
            case REQUEST_STREAM:
                return OperationType.RequestStream;
            case STREAM:
                return OperationType.Stream;
            default:
                throw new RuntimeException("Unknown operation type " + operationType);
        }
    }
}
