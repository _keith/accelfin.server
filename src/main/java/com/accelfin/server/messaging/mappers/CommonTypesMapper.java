package com.accelfin.server.messaging.mappers;

import com.accelfin.server.messaging.dtos.DecimalDto;
import com.accelfin.server.messaging.dtos.TimestampDto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.Instant;

public class CommonTypesMapper {
    // note there was an attempt to make this a proper proto type, check history (c7e2d6b120e91970776c5442c9ff2db38056e82f)
    // but backed it out. Leaving these here for now as it's sensible to have a common port of call for this mapping
    public static DecimalDto mapBigDecimalToDto(BigDecimal d) {
        return DecimalDto.newBuilder()
                .setUnscaledValue(d.unscaledValue().longValue())
                .setScale(d.scale())
                .build();
    }

    public static BigDecimal  mapBigDecimalFromDto(DecimalDto dto) {
        return new BigDecimal(BigInteger.valueOf(dto.getUnscaledValue()), dto.getScale());
    }

    public static BigDecimal  mapBigDecimalFromDto(String s) {
        return new BigDecimal(s);
    }

    public static TimestampDto mapInstantToDto(Instant time) {
        return TimestampDto.newBuilder()
                .setSeconds(time.getEpochSecond())
                .setNanos(time.getNano())
                .build();
    }

    public static Instant mapInstantFromDto(TimestampDto time) {
        return Instant.ofEpochSecond(time.getSeconds(), time.getNanos());
    }
}
