package com.accelfin.server.messaging.mappers;

import com.accelfin.server.messaging.MessageEnvelope;
import com.accelfin.server.messaging.events.InboundMessageEvent;

public abstract class InboundMessageEventMapperBase implements InboundMessageEventMapper {
    @Override
    public final InboundMessageEvent mapToEvent(MessageEnvelope envelope) {

        // TODO add inbound message validation
        // see https://github.com/KeithWoods/devshop.dealerapp.v2/issues/172
        InboundMessageEvent event = tryMapToEvent(envelope);
        if(event == null) {
            throw new RuntimeException("Could not map event for operation type " + envelope.getOperationName());
        }
        MessageEnvelopeMapper.mapBaseFields(event, envelope);
        return event;
    }

    protected abstract InboundMessageEvent tryMapToEvent(MessageEnvelope envelope);
}
