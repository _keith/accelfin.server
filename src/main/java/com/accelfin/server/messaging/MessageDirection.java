package com.accelfin.server.messaging;

public enum MessageDirection {
    Request,
    Response
}
