package com.accelfin.server.messaging;

import com.accelfin.server.messaging.config.operations.OperationConfig;
import com.google.protobuf.Message;

public class OutboundMessage {
    private String _operationName;
    private String _correlationId;
    private MessageDirection _messageDirection;
    private OperationConfig _operationConfig;
    private String[] _sessionIds;
    private Message _payload;
    private String _replyTo;
    private String _error;
    private Boolean _hasCompleted;
    private Integer _ttl;

    public OutboundMessage(
            String operationName,
            String correlationId,
            MessageDirection messageDirection,
            OperationConfig operationConfig,
            Message payload,
            String replyTo,
            String error,
            Boolean hasCompleted,
            Integer ttl,
            String... sessionTokens
        ) {
        _operationName = operationName;
        _correlationId = correlationId;
        _messageDirection = messageDirection;
        _operationConfig = operationConfig;
        _sessionIds = sessionTokens;
        _payload = payload;
        _replyTo = replyTo;
        _error = error;
        _hasCompleted = hasCompleted;
        _ttl = ttl;
    }

    public String getOperationName() {
        return _operationName;
    }

    public String getCorrelationId() {
        return _correlationId;
    }

    public String[] getSessionIds() {
        return _sessionIds;
    }

    public Message getPayload() {
        return _payload;
    }

    public String getReplyTo() {
        return _replyTo;
    }

    public String getError() {
        return _error;
    }

    public Boolean getHasCompleted() {
        return _hasCompleted;
    }

    public Boolean getHasSessionIds() {
        return _sessionIds != null && _sessionIds.length > 0;
    }

    public Integer getTtl() {
        return _ttl;
    }

    public MessageDirection getMessageDirection() {
        return _messageDirection;
    }

    public OperationConfig getOperationConfig() {
        return _operationConfig;
    }
}
