package com.accelfin.server.messaging;

public class OutboundMessageBuilder<TResponse> {
    private TResponse _payload;
    private String[] _sessionTokens;

    public OutboundMessageBuilder<TResponse> setPayload(TResponse payload) {
        _payload = payload;
        return this;
    }

    public OutboundMessageBuilder<TResponse> setSessionTokens(String... sessionTokens) {
        _sessionTokens = sessionTokens;
        return this;
    }

    public OutboundMessage build() {
        return new OutboundMessage(_payload, _sessionTokens);
    }
}
