package com.accelfin.server.messaging.config.operations;

/**
 * Defines where the operation is computed
 */
public enum OperationOwnership {
    /**
     * local to the current service instance. I.e. this service owns this operation.
     */
    OwnedByThisService,
    /**
     * A remote service instance owns this operation
     */
    OwnedByRemoteService
}
