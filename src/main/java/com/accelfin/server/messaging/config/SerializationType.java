package com.accelfin.server.messaging.config;

public enum SerializationType {
    Json,
    Binary
}
