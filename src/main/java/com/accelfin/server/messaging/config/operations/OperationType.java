package com.accelfin.server.messaging.config.operations;

public enum OperationType {
    RPC,
    RequestStream,
    Stream
}