package com.accelfin.server.messaging.config.operations;


import com.accelfin.server.messaging.MessageDirection;

public class DefaultOperationMqNamingStrategy implements MqNamingStrategy {
    private OperationConfig _operationConfig;

    public DefaultOperationMqNamingStrategy(OperationConfig operationConfig) {
        _operationConfig = operationConfig;
    }

    @Override
    public String getQueueName() {
        return String.format("%s.%s-queue.%s",
                _operationConfig.getServiceType(),
                _operationConfig.getOperationName(),
                _operationConfig.getServiceId()
        );
    }

    @Override
    public String getRequestRoutingKey() {
        return formatRoutingKey(MessageDirection.Request, null);
    }

    @Override
    public String getResponseRoutingKey() {
        return formatRoutingKey(MessageDirection.Response, null);
    }

    @Override
    public String getResponseRoutingKey(String sessionId) {
        return formatRoutingKey(MessageDirection.Response, sessionId);
    }

    private String formatRoutingKey(MessageDirection direction, String sessionId) {
        return String.format("%s.%s-%s.%s%s",
                _operationConfig.getServiceType(),
                _operationConfig.getOperationName(),
                direction == MessageDirection.Request ? "request" : "response",
                _operationConfig.getServiceId(),
                sessionId != null ? "." + sessionId : ""
        );
    }
}
