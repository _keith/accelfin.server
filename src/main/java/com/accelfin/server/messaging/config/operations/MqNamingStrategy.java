package com.accelfin.server.messaging.config.operations;

public interface MqNamingStrategy {
    String getQueueName();

    String getRequestRoutingKey();

    String getResponseRoutingKey();

    String getResponseRoutingKey(String sessionId);
}
