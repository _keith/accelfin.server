package com.accelfin.server.messaging;

public interface OutboundMessageHandler {
    void handle(OutboundMessage outboundMessage);
}
