package com.accelfin.server;

import com.accelfin.server.messaging.Connections;
import com.esp.PreEventProcessor;
import com.esp.disposables.DisposableBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class ServiceModelBase extends DisposableBase implements PreEventProcessor {

    private static Logger logger = LoggerFactory.getLogger(ServiceModelBase.class);

    private Connections _connections;
    private Clock _clock;

    public ServiceModelBase(Clock clock, Connections connections) {
        _clock = clock;
        _connections = connections;
    }

    public Connections getConnections() {
        return _connections;
    }

    @Override
    public void preProcess() {
        _connections.preProcess();
    }

    public void observeEvents() {
        _connections.observeEvents();
        _clock.observeEvents();
    }
}