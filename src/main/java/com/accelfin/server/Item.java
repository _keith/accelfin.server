package com.accelfin.server;

/**
 * Used in lambdas when you need to close over a modifiable variable
 * @param <T>
 */
public class Item<T> {
    private T _item;

    public Item() {
    }

    public Item(T item) {
        _item = item;
    }

    public T getItem() {
        return _item;
    }

    public void setItem(T item) {
        _item = item;
    }
}
