package com.accelfin.server;

/**
 * A users session is identified by both a session id and the connection that session is active on.
 *
 * Note that the sessionId can be null or empty.
 */
public class SessionToken {
    private final String _sessionId;
    private final String _connectionId;

    public SessionToken(String sessionId, String connectionId) {
        _sessionId = sessionId;
        _connectionId = connectionId;
    }

    public String getSessionId() {
        return _sessionId;
    }

    public String getConnectionId() {
        return _connectionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SessionToken)) return false;

        SessionToken that = (SessionToken) o;

        if (_sessionId != null ? !_sessionId.equals(that._sessionId) : that._sessionId != null) return false;
        return _connectionId != null ? _connectionId.equals(that._connectionId) : that._connectionId == null;

    }

    @Override
    public int hashCode() {
        int result = _sessionId != null ? _sessionId.hashCode() : 0;
        result = 31 * result + (_connectionId != null ? _connectionId.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "SessionToken{" +
                "_sessionId='" + _sessionId + '\'' +
                ", _connectionId=" + _connectionId +
                '}';
    }
}
