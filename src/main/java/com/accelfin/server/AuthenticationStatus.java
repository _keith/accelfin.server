package com.accelfin.server;

public enum  AuthenticationStatus {
    Authenticated,
    Unauthenticated,
}
