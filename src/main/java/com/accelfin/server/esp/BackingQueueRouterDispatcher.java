package com.accelfin.server.esp;

import com.esp.RouterDispatcher;
import com.esp.functions.Action0;
import org.slf4j.LoggerFactory;

import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

public class BackingQueueRouterDispatcher implements RouterDispatcher {

    private final ExecutorService _routerExecutorService;
    private String _threadName = "RouterThread";

    public BackingQueueRouterDispatcher() {
        // newSingleThreadExecutor uses a LinkedBlockingQueue, i..e manages a queue of items to processes
        _routerExecutorService = Executors.newSingleThreadExecutor(new RouterThreadFactory(_threadName));
    }

    @Override
    public boolean checkAccess() {
        return Objects.equals(Thread.currentThread().getName(), _threadName);
    }

    @Override
    public void ensureAccess() {
        if(!checkAccess()) {
            throw new IllegalStateException("Invalid thread access, not on thread " + _threadName);
        }
    }

    @Override
    public void dispatch(Action0 action) {
        _routerExecutorService.submit(action::call);
    }

    @Override
    public void dispose() {
        throw new IllegalStateException("not implemented");
    }
}

class RouterThreadFactory implements ThreadFactory {

    private String _threadName;

    public RouterThreadFactory(String threadName) {
        _threadName = threadName;
    }

    @Override
    public Thread newThread(Runnable r) {
        Thread t = new Thread(r);
        t.setName(_threadName);
        t.setUncaughtExceptionHandler((t1, e) -> LoggerFactory.getLogger(t1.getName()).error(e.getMessage(), e));
        return t;
    }
}