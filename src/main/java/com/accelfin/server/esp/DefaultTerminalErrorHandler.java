package com.accelfin.server.esp;

import com.esp.TerminalErrorHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class DefaultTerminalErrorHandler implements TerminalErrorHandler {
    private static Logger logger = LoggerFactory.getLogger(DefaultTerminalErrorHandler.class);

    @Override
    public void onError(Exception exception) {
        logger.error("Unhandled error in router handler", exception);
    }
}
