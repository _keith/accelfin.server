package com.accelfin.server;

class Guard
{
    public static void ArgumentRequires(boolean test) {
        if(!test){
            throw new IllegalArgumentException();
        }
    }
}